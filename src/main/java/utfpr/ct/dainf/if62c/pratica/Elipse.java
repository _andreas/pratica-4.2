/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Andreas
 */
public class Elipse implements FiguraComEixos {

    private double eixo1, eixo2, r, s;

    @Override
    public String getNome() {
        return this.getClass().getSimpleName();
    }

    public Elipse(double r, double s) {
        this.eixo1 = r * 2;
        this.eixo2 = s * 2;
        this.r = r;
        this.s = s;
    }

    @Override
    public double getArea() {
        return (Math.PI * r * s);
    }

    @Override
    public double getEixoMenor() {
        if (eixo1 <= eixo2) {
            return eixo1;
        }
        return eixo2;
    }

    @Override
    public double getEixoMaior() {
        if (eixo1 >= eixo2) {
            return eixo1;
        }
        return eixo2;
    }

    @Override
    public double getPerimetro() {
        return (Math.PI * ((3.0 * (r + s)) - Math.sqrt((3.0 * r + s) * (r + 3.0 * s))));
    }

}
