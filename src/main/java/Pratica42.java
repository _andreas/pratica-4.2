
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Andreas
 */
public class Pratica42 {

    public static void main(String[] args) {
        Circulo c = new Circulo(4.0);
        Elipse e = new Elipse(4.0, 8.0);
        System.out.println("Area do circulo = " + c.getArea());
        System.out.println("Perimetro do circulo = " + c.getPerimetro());
        System.out.println("Area da elipse = " + e.getArea());
        System.out.println("Perimetro da elipse = " + e.getPerimetro());
    }

}
